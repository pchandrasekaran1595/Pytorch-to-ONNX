import sys

from torch import randn, onnx
from models import get_model

import warnings
warnings.filterwarnings("ignore")


def main():
    name = "mobilenet_v3_small"
    size = 224
    opset_version = 9
    batch_size = 1

    args_1 = "--name"
    args_2 = "--size"
    args_3 = "--opver"

    if args_1 in sys.argv: name = sys.argv[sys.argv.index(args_1) + 1]
    if args_2 in sys.argv: size = int(sys.argv[sys.argv.index(args_2) + 1])
    if args_3 in sys.argv: opset_version = int(sys.argv[sys.argv.index(args_3) + 1])


    model = get_model(name)
    dummy = randn(batch_size, 3, size, size)

    onnx.export(model=model, 
                args=dummy, 
                f="./op/{}.onnx".format(name), 
                input_names=["input"], 
                output_names=["output"], 
                opset_version=opset_version,
                export_params=True,
                training=False,
                dynamic_axes={
                    "input" : {0 : "batch_size"},
                    "output" : {0 : "batch_size"}
                }
    )


if __name__ == "__main__":
    sys.exit(main() or 0)
