- Python Script to convert Pytorch Classifier Models to Caffe Model

- Command Line Arguments
    1. `--name`
    2. `--size`

- Can convert mobilenet models to ONNX as well, but doesn't work when used to perform inference since it cannot create some layers. 